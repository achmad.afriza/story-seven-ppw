from django.shortcuts import render, redirect
from django.contrib import messages
from django.core import validators
from django.urls import reverse
import datetime
from .models import Status
from .forms import StatusInput


# Create your views here.

def index(request):
    try:
        del request.session['name']
        del request.session['status']
    except:
        pass
    
    all_status = Status.objects.all().order_by('-time')

    if request.method == 'POST':
        status_form = StatusInput(request.POST)

        if status_form.is_valid():
            # status_form.save()
            # return redirect(reverse('homepage:index'))
            request.session['name'] = status_form.cleaned_data['name']
            request.session['status'] = status_form.cleaned_data['status']
            return redirect(reverse('homepage:confirm'))
    else:
        status_form = StatusInput()

    context = {
        'all_status': all_status,
        'status_form': status_form,
        'nav': [
            [reverse('homepage:index'), 'Home'],
            ['https://story5ppw-achmadafriza.herokuapp.com/', 'About Me'],
        ]
    }

    return render(request, 'homepage.html', context)

def confirm(request):
    if request.session.has_key('name') & request.session.has_key('status'):
        name = request.session['name']
        status = request.session['status']

        context = {
            'nav': [
                [reverse('homepage:index'), 'Home'],
                ['https://story5ppw-achmadafriza.herokuapp.com/', 'About Me'],
            ],
            'name': name,
            'status': status,
            'time' : datetime.datetime.now()
        }
        if request.method == "POST":
            Status.objects.create(name=name, status=status)

            return redirect(reverse('homepage:index'))
        
        else:
            return render(request, 'confirm.html', context)
    else:
        return redirect(reverse('homepage:index'))