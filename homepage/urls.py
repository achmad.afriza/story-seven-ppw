from django.urls import path, include
from django.conf.urls import url
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('confirm/', views.confirm, name='confirm')
]