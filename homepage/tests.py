from django.http import HttpRequest
from django.test import TestCase, LiveServerTestCase
from django.urls import reverse, resolve
from django.conf import settings
from importlib import import_module

from .apps import HomepageConfig
from .views import index, confirm
from .models import Status
from .forms import StatusInput
from django.contrib import admin

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class HomePageTests(TestCase):
    def setUp(self):
        # http://code.djangoproject.com/ticket/10899
        settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key

    def test_apps(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
    
    def test_homepage_status_code(self):
        response = self.client.get('')
        self.assertEquals(response.status_code, 200)

    def test_view_url_by_name(self):
        response = self.client.get(reverse('homepage:index'))
        self.assertEquals(response.status_code, 200)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('homepage:index'))
        self.assertTemplateUsed(response, 'homepage.html')

    def test_homepage_contains_correct_html(self):
        response = self.client.get('')
        self.assertContains(response, "<title>Afriza's StorySeven</title>")

    def test_homepage_does_not_contain_incorrect_html(self):
        response = self.client.get('')
        self.assertNotContains(response, '{% extends "base.html" %}')
    
    def test_homepage_uses_correct_view(self):
        func = resolve('/')
        self.assertEqual(index, func.func)
    
    def test_model_is_in_admin(self):
        self.assertEqual(Status in admin.site._registry, True)
    
    def test_model_can_create_new_status(self):
        status = Status.objects.create(name='rija', status='Hi!')

        count = Status.objects.all().count()
        self.assertEqual(count, 1)
    
    def test_form_validation_for_blank_items(self):
        form = StatusInput(data={'name':'', 'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
    
    # Different Behaviour as after POST, it is redirected to confirmation page
    # def test_post_success_and_render_the_result(self):
    #     context = {'name':'rija', 'status' : 'Hi!'}
    #     response_post = self.client.post('', context)
    #     self.assertEqual(response_post.status_code, 302)

    #     response = self.client.get('')
    #     self.assertEqual(response.context['all_status'].get(status=context['status']).status, context['status'])

    def test_confirm_url_by_name(self):
        response = self.client.get(reverse('homepage:confirm'))
        self.assertEquals(response.status_code, 302)

    def test_confirm_uses_correct_template(self):
        session = self.session
        session['name'] = 'afriza'
        session['status'] = '1234567890'
        session.save()

        response = self.client.get(reverse('homepage:confirm'))
        self.assertTemplateUsed(response, 'confirm.html')

    def test_confirm_contains_correct_html(self):
        session = self.session
        session['name'] = 'afriza'
        session['status'] = '1234567890'
        session.save()

        response = self.client.get(reverse('homepage:confirm'))
        self.assertContains(response, '<input class="btn btn-primary" type="submit" value="Post">')

    def test_confirm_does_not_contain_incorrect_html(self):
        session = self.session
        session['name'] = 'afriza'
        session['status'] = '1234567890'
        session.save()

        response = self.client.get(reverse('homepage:confirm'))
        self.assertNotContains(response, '{% extends "base.html" %}')
    
    def test_confirm_uses_correct_view(self):
        session = self.session
        session['name'] = 'afriza'
        session['status'] = '1234567890'
        session.save()

        func = resolve(reverse('homepage:confirm'))
        self.assertEqual(confirm, func.func)
    
    def test_form_validation_for_filled_items(self) :
        session = self.session
        session['name'] = 'afriza'
        session['status'] = '1234567890'
        session.save()

        response = self.client.post(reverse('homepage:confirm'))
        response_content = response.content.decode()
        self.assertIn(response_content, 'afriza')

    def test_form_name(self) :
        session = self.session
        session['name'] = 'afriza'
        session['status'] = '1234567890'
        session.save()

        response = self.client.post(reverse('homepage:confirm'))
        status = Status.objects.get(pk=1)
        self.assertEqual(str(status), status.status)

class Story7_FunctionalTest(LiveServerTestCase):
    def setUp(self) :
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')
    
    def tearDown(self) :
        self.driver.quit()
        super().tearDown()
    
    def test_functional_post(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_class_name('statusform').send_keys('0987654321')
        self.driver.find_element_by_class_name('nameform').send_keys('achmad')
        self.driver.find_element_by_class_name('btn-primary').click()

        self.driver.find_element_by_class_name('btn-primary').click()
        response_content = self.driver.page_source
        self.assertIn('achmad', response_content)
    
    def test_functional_abort(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_class_name('statusform').send_keys('0987654321')
        self.driver.find_element_by_class_name('nameform').send_keys('achmad')
        self.driver.find_element_by_class_name('btn-primary').click()

        self.driver.find_element_by_class_name('btn-danger').click()
        response_content = self.driver.page_source
        self.assertNotIn('wibawa', response_content)