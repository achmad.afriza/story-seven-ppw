from django import forms
from .models import Status

class StatusInput(forms.ModelForm):
    class Meta:
        model = Status
        fields = {
            'name',
            'status'
        }
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'placeholder': 'JoniBonJovi',
                    'class': 'nameform'
                }
            ),
            'status': forms.TextInput(
                attrs={
                    'placeholder': 'I Wanna go Home...',
                    'class': 'statusform'
                }
            )
        }
    
    def __init__(self, *args, **kwargs):
        super(StatusInput, self).__init__(*args, **kwargs)
        self.fields['status'].label = ''
        self.fields['name'].label = ''